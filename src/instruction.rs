use std::io::{self, Write};

use crate::{program::ConstantPoolIndex};

pub const LABEL_OPCODE: u8 = 0x00;
pub const LITERAL_OPCODE: u8 = 0x01;
pub const PRINT_OPCODE: u8 = 0x02;
pub const ARRAY_OPCODE: u8 = 0x03;
pub const OBJECT_OPCODE: u8 = 0x04;
pub const GET_FIELD_OPCODE: u8 = 0x05;
pub const SET_FIELD_OPCODE: u8 = 0x06;
pub const CALL_METHOD_OPCODE: u8 = 0x07;
pub const CALL_FUNCTION_OPCODE: u8 = 0x08;
pub const SET_LOCAL_OPCODE: u8 = 0x09;
pub const GET_LOCAL_OPCODE: u8 = 0x0A;
pub const SET_GLOBAL_OPCODE: u8 = 0x0B;
pub const GET_GLOBAL_OPCODE: u8 = 0x0C;
pub const BRANCH_OPCODE: u8 = 0x0D;
pub const JUMP_OPCODE: u8 = 0x0E;
pub const RETURN_OPCODE: u8 = 0x0F;
pub const DROP_OPCODE: u8 = 0x10;

#[derive(Clone, PartialEq, Eq, Hash)]
pub enum Instruction {
    Label(ConstantPoolIndex),
    Literal(ConstantPoolIndex),
    GetLocal(u16),
    SetLocal(u16),
    GetGlobal(ConstantPoolIndex),
    SetGlobal(ConstantPoolIndex),
    CallFunction {
        name: ConstantPoolIndex,
        arguments: u8,
    },
    Return,
    Jump(ConstantPoolIndex),
    Branch(ConstantPoolIndex),
    Print {
        format: ConstantPoolIndex,
        arguments: u8,
    },
    Array,
    Object(ConstantPoolIndex),
    GetField(ConstantPoolIndex),
    SetField(ConstantPoolIndex),
    CallMethod {
        name: ConstantPoolIndex,
        arguments: u8,
    },
    Drop,
}

impl Instruction {
    pub fn write_as_bytes<W: Write>(&self, dest: &mut W) -> io::Result<()> {
        match self {
            Instruction::Label(index) => {
                dest.write(&LABEL_OPCODE.to_le_bytes())?;
                dest.write(&index.to_le_bytes())?;
            }
            Instruction::Literal(index) => {
                dest.write(&LITERAL_OPCODE.to_le_bytes())?;
                dest.write(&index.to_le_bytes())?;
            }
            Instruction::GetLocal(index) => {
                dest.write(&GET_LOCAL_OPCODE.to_le_bytes())?;
                dest.write(&index.to_le_bytes())?;
            }
            Instruction::SetLocal(index) => {
                dest.write(&SET_LOCAL_OPCODE.to_le_bytes())?;
                dest.write(&index.to_le_bytes())?;
            }
            Instruction::GetGlobal(index) => {
                dest.write(&GET_GLOBAL_OPCODE.to_le_bytes())?;
                dest.write(&index.to_le_bytes())?;
            }
            Instruction::SetGlobal(index) => {
                dest.write(&SET_GLOBAL_OPCODE.to_le_bytes())?;
                dest.write(&index.to_le_bytes())?;
            }
            Instruction::CallFunction { name, arguments } => {
                dest.write(&CALL_FUNCTION_OPCODE.to_le_bytes())?;
                dest.write(&name.to_le_bytes())?;
                dest.write(&arguments.to_le_bytes())?;
            }
            Instruction::Return => {
                dest.write(&RETURN_OPCODE.to_le_bytes())?;
            }
            Instruction::Jump(index) => {
                dest.write(&JUMP_OPCODE.to_le_bytes())?;
                dest.write(&index.to_le_bytes())?;
            }
            Instruction::Branch(index) => {
                dest.write(&BRANCH_OPCODE.to_le_bytes())?;
                dest.write(&index.to_le_bytes())?;
            }
            Instruction::Print { format, arguments } => {
                dest.write(&PRINT_OPCODE.to_le_bytes())?;
                dest.write(&format.to_le_bytes())?;
                dest.write(&arguments.to_le_bytes())?;
            }
            Instruction::Array => {
                dest.write(&ARRAY_OPCODE.to_le_bytes())?;
            }
            Instruction::Object(index) => {
                dest.write(&OBJECT_OPCODE.to_le_bytes())?;
                dest.write(&index.to_le_bytes())?;
            }
            Instruction::GetField(index) => {
                dest.write(&GET_FIELD_OPCODE.to_le_bytes())?;
                dest.write(&index.to_le_bytes())?;
            }
            Instruction::SetField(index) => {
                dest.write(&SET_FIELD_OPCODE.to_le_bytes())?;
                dest.write(&index.to_le_bytes())?;
            }
            Instruction::CallMethod { name, arguments } => {
                dest.write(&CALL_METHOD_OPCODE.to_le_bytes())?;
                dest.write(&name.to_le_bytes())?;
                dest.write(&arguments.to_le_bytes())?;
            }
            Instruction::Drop => {
                dest.write(&DROP_OPCODE.to_le_bytes())?;
            }
        };

        io::Result::Ok(())
    }
}
