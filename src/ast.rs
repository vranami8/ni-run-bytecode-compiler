use serde::Deserialize;

// NOTE: taken from FML refrence because it already uses serde and I was too lazy to write all of this
#[derive(Deserialize, Clone)]
pub enum AST {
    Null,
    Boolean(bool),
    Integer(i32),

    Variable {
        name: String,
        value: Box<AST>,
    },

    Array {
        size: Box<AST>,
        value: Box<AST>,
    },

    Object {
        extends: Box<AST>,
        members: Vec<Box<AST>>,
    },

    AccessVariable {
        name: String,
    },

    AccessField {
        object: Box<AST>,
        field: String,
    },

    AccessArray {
        array: Box<AST>,
        index: Box<AST>,
    },

    AssignVariable {
        name: String,
        value: Box<AST>,
    },

    AssignField {
        object: Box<AST>,
        field: String,
        value: Box<AST>,
    },

    AssignArray {
        array: Box<AST>,
        index: Box<AST>,
        value: Box<AST>,
    },

    Function {
        name: String,
        parameters: Vec<String>,
        body: Box<AST>,
    },

    CallFunction {
        name: String,
        arguments: Vec<Box<AST>>,
    },

    CallMethod {
        object: Box<AST>,
        name: String,
        arguments: Vec<Box<AST>>,
    },

    Top(Vec<Box<AST>>),
    Block(Vec<Box<AST>>),
    Loop {
        condition: Box<AST>,
        body: Box<AST>,
    },
    Conditional {
        condition: Box<AST>,
        consequent: Box<AST>,
        alternative: Box<AST>,
    },

    Print {
        format: String,
        arguments: Vec<Box<AST>>,
    },
}

impl AST {
    pub fn is_array_comprehensible(&self) -> bool {
        match self {
            AST::Null => false,
            AST::Boolean(_) => false,
            AST::Integer(_) => false,
            AST::AccessVariable { .. } => false,
            AST::AccessField { .. } => false,
            AST::AssignVariable { value, .. } => value.is_array_comprehensible(),
            AST::AssignField { value, .. } => value.is_array_comprehensible(),
            AST::Block(nodes) => nodes.iter().any(|node| node.is_array_comprehensible()),
            AST::Conditional {
                condition,
                consequent,
                alternative,
            } => {
                condition.is_array_comprehensible()
                    || consequent.is_array_comprehensible()
                    || alternative.is_array_comprehensible()
            }
            _ => true,
        }
    }
}
