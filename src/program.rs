use std::{
    collections::HashMap,
    io::{self, Write},
};

use crate::program_object::{Code, ProgramObject};

pub type ConstantPoolIndex = u16;

const ENTRY_POINT_NAME: &str = "λ:";

pub struct Program {
    constant_pool: HashMap<ProgramObject, ConstantPoolIndex>,
    global_vars: HashMap<String, ConstantPoolIndex>,
    functions: Vec<ConstantPoolIndex>,
    entry_point: Option<ConstantPoolIndex>,
}

impl Program {
    pub fn new() -> Self {
        Self {
            constant_pool: HashMap::new(),
            global_vars: HashMap::new(),
            functions: Vec::new(),
            entry_point: None,
        }
    }

    fn add_program_object(&mut self, object: ProgramObject) -> ConstantPoolIndex {
        let index = self.constant_pool.len();

        if index == ConstantPoolIndex::MAX as usize {
            panic!(
                "Constant pool maximum size exceeded, constant pool can hold maximum of {} objects",
                ConstantPoolIndex::MAX
            );
        }

        self.constant_pool
            .insert(object, index as ConstantPoolIndex);

        index as ConstantPoolIndex
    }

    pub fn register_global_var(&mut self, name: &String, slot_index: ConstantPoolIndex) {
        self.global_vars.insert(name.clone(), slot_index);
    }

    pub fn add_new_global_variable(&mut self, name: &String) -> ConstantPoolIndex {
        let name_index =
            self.get_or_assign_constant_pool_index(&ProgramObject::String(name.clone()));
    
        let slot_index = self.add_program_object(ProgramObject::Slot(name_index));

        match self.global_vars.insert(name.clone(), slot_index) {
            Some(_) => panic!("Variable '{}' has been declared multiple times", name),
            None => {}
        };

        name_index
    }

    pub fn get_or_assign_constant_pool_index(
        &mut self,
        object: &ProgramObject,
    ) -> ConstantPoolIndex {
        match self.constant_pool.get(object) {
            Some(index) => *index,
            None => self.add_program_object(object.clone()),
        }
    }

    pub fn add_function(&mut self, method_index: ConstantPoolIndex) {
        self.functions.push(method_index);
    }

    pub fn add_method(&mut self, name: &String, arguments: u8, locals: u16, code: Code) -> ConstantPoolIndex {
        let name_index =
            self.get_or_assign_constant_pool_index(&ProgramObject::String(name.clone()));

        self.add_program_object(ProgramObject::Method {
            name: name_index,
            arguments,
            locals,
            code,
        })
    }

    pub fn add_entry_point(&mut self, code: Code, locals: u16) {
        let name_index = self.get_or_assign_constant_pool_index(&ProgramObject::String(
            String::from(ENTRY_POINT_NAME),
        ));

        let function_index = self.add_program_object(ProgramObject::Method {
            name: name_index,
            arguments: 0,
            locals,
            code,
        });

        self.entry_point = Some(function_index);
    }

    pub fn write_as_bytes<W: Write>(&self, dest: &mut W) -> io::Result<()> {
        let globals_len = self.global_vars.len() + self.functions.len();

        if globals_len > u16::MAX as usize {
            panic!("Maximum number of globals exceededd");
        }

        dest.write(&(self.constant_pool.len() as u16).to_le_bytes())?;

        let mut objects: Vec<(&ProgramObject, &ConstantPoolIndex)> =
            self.constant_pool.iter().collect();

        objects.sort_by_key(|(_, &index)| index);

        for (object, _) in objects {
            object.write_as_bytes(dest)?;
        }

        dest.write(&(globals_len as u16).to_le_bytes())?;

        for slot_index in self.global_vars.values() {
            dest.write(&slot_index.to_le_bytes())?;
        }

        for function_index in self.functions.iter() {
            dest.write(&function_index.to_le_bytes())?;
        }

        let entry_point_index = self
            .entry_point
            .unwrap_or_else(|| panic!("No entry point specified"));

        dest.write(&entry_point_index.to_le_bytes())?;

        io::Result::Ok(())
    }
}
