use std::result::Result;

pub trait PanicOnErr {
    fn panic_on_err(&self);
}

impl<T, E> PanicOnErr for Result<T, E> {
    fn panic_on_err(&self) {
        match self {
            Ok(_) => (),
            Err(_) => panic!("Unexpected error occured"),
        }
    }
}