use std::io;

use ast::AST;
use compiler::BytecodeCompiler;

mod ast;
mod compiler;
mod instruction;
mod program;
mod helpers;
mod program_object;

#[cfg(test)]
mod tests;

fn main() {
    let ast: AST = serde_json::from_reader(io::stdin())
        .unwrap_or_else(|err| panic!("JSON parsing error: {}", err.to_string()));

    BytecodeCompiler::new().compile(&ast, &mut io::stdout());
}
