use std::io::{self, Write};

use crate::{instruction::Instruction, program::ConstantPoolIndex};

pub type Code = Vec<Instruction>;

pub const INTEGER_TAG: u8 = 0x00;
pub const NULL_TAG: u8 = 0x01;
pub const STRING_TAG: u8 = 0x02;
pub const METHOD_TAG: u8 = 0x03;
pub const SLOT_TAG: u8 = 0x04;
pub const CLASS_TAG: u8 = 0x05;
pub const BOOLEAN_TAG: u8 = 0x06;

#[derive(Clone, PartialEq, Eq, Hash)]
pub enum ProgramObject {
    Integer(i32),
    Boolean(bool),
    Null,
    String(String),
    Slot(ConstantPoolIndex),
    Method {
        name: ConstantPoolIndex,
        arguments: u8,
        locals: u16,
        code: Code,
    },
    Class(Vec<ConstantPoolIndex>),
}

impl ProgramObject {
    pub fn write_as_bytes<W: Write>(&self, dest: &mut W) -> io::Result<()> {
        match self {
            ProgramObject::Integer(value) => {
                dest.write(&INTEGER_TAG.to_le_bytes())?;
                dest.write(&value.to_le_bytes())?;
            }
            ProgramObject::Boolean(value) => {
                dest.write(&BOOLEAN_TAG.to_le_bytes())?;

                let byte_value: [u8; 1] = if *value { [1] } else { [0] };

                dest.write(&byte_value)?;
            }
            ProgramObject::Null => {
                dest.write(&NULL_TAG.to_le_bytes())?;
            }
            ProgramObject::String(string) => {
                dest.write(&STRING_TAG.to_le_bytes())?;
                dest.write(&(string.len() as u32).to_le_bytes())?;
                dest.write(string.as_bytes())?;
            }
            ProgramObject::Slot(index) => {
                dest.write(&SLOT_TAG.to_le_bytes())?;
                dest.write(&index.to_le_bytes())?;
            }
            ProgramObject::Method {
                name,
                arguments,
                locals,
                code,
            } => {
                if code.len() > u32::MAX as usize {
                    panic!("Maximum number of instructions in a method exceed");
                }

                dest.write(&METHOD_TAG.to_le_bytes())?;
                dest.write(&name.to_le_bytes())?;
                dest.write(&arguments.to_le_bytes())?;
                dest.write(&locals.to_le_bytes())?;
                dest.write(&(code.len() as u32).to_le_bytes())?;

                for instruction in code {
                    instruction.write_as_bytes(dest)?;
                }
            }
            ProgramObject::Class(members) => {
                dest.write(&CLASS_TAG.to_le_bytes())?;
                dest.write(&(members.len() as u16).to_le_bytes())?;

                for index in members {
                    dest.write(&index.to_le_bytes())?;
                }
            }
        }

        io::Result::Ok(())
    }
}
