use std::{fs::File, io};

use crate::{ast::AST, compiler::BytecodeCompiler};

#[test]
fn test() {
    let reader = File::open("test.ast.json").unwrap();

    let ast: AST = serde_json::from_reader(reader)
        .unwrap_or_else(|err| panic!("JSON parsing error: {}", err.to_string()));

    BytecodeCompiler::new().compile(&ast, &mut io::stdout());
}
