use std::{collections::HashMap, io::Write};

use crate::{
    ast::AST,
    helpers::PanicOnErr,
    instruction::Instruction,
    program::{ConstantPoolIndex, Program},
    program_object::{Code, ProgramObject},
};

type LocalVars = HashMap<String, u16>;

#[derive(Clone)]
struct CompilationContext {
    pub code: Code,
    pub environments: Vec<LocalVars>,
    pub max_locals_count: u16,
    pub next_var_index: u16,
}

impl CompilationContext {
    pub fn new() -> Self {
        Self {
            code: Code::new(),
            environments: Vec::new(),
            max_locals_count: 0,
            next_var_index: 0,
        }
    }

    pub fn add_local_var(&mut self, name: &String) -> u16 {
        let index = self.next_var_index;
        self.next_var_index = self.next_var_index + 1;

        match self
            .environments
            .last_mut()
            .unwrap_or_else(|| panic!("Cannot add variable to empty environment"))
            .insert(name.clone(), index)
        {
            Some(_) => panic!("Redeclaration of variable '{}'", name),
            None => {}
        };

        if index >= self.max_locals_count {
            self.max_locals_count = index + 1
        }

        index
    }

    pub fn get_local_var_index(&self, name: &String) -> Option<u16> {
        self.environments
            .iter()
            .rev()
            .find_map(|env| env.get(name).map(|index| *index))
    }

    pub fn extend_env(&mut self) {
        self.environments.push(LocalVars::new());
    }

    pub fn pop_env(&mut self) {
        let popped_env = self
            .environments
            .pop()
            .unwrap_or_else(|| panic!("Cannot pop empty environment frame"));

        self.next_var_index = self.next_var_index - popped_env.len() as u16;
    }

    pub fn is_global(&self) -> bool {
        self.environments.is_empty()
    }
}

pub struct BytecodeCompiler {
    program: Program,
    next_label_id: u16,
}

impl BytecodeCompiler {
    pub fn new() -> Self {
        Self {
            program: Program::new(),
            next_label_id: 0,
        }
    }

    fn get_label_id(&mut self) -> u16 {
        let id = self.next_label_id;
        self.next_label_id = self.next_label_id + 1;
        id
    }

    fn compile_literal(&mut self, context: &mut CompilationContext, literal_value: &ProgramObject) {
        let literal_index = self
            .program
            .get_or_assign_constant_pool_index(literal_value);

        context.code.push(Instruction::Literal(literal_index))
    }

    fn compile_local_variable(
        &mut self,
        context: &mut CompilationContext,
        name: &String,
        value: &AST,
    ) {
        self.compile_ast_node(context, value);

        let local_var_index = context.add_local_var(name);

        context.code.push(Instruction::SetLocal(local_var_index));
    }

    fn compile_global_variable(
        &mut self,
        context: &mut CompilationContext,
        value: &AST,
        name: &String,
    ) -> ConstantPoolIndex {
        self.compile_ast_node(context, value);
        let name_index = self.program.add_new_global_variable(name);
        context.code.push(Instruction::SetGlobal(name_index));

        name_index
    }

    fn compile_variable(&mut self, context: &mut CompilationContext, name: &String, value: &AST) {
        if !context.is_global() {
            self.compile_local_variable(context, name, value);
            return;
        }

        self.compile_global_variable(context, value, name);
    }

    fn compile_access_variable(&mut self, context: &mut CompilationContext, name: &String) {
        match context.get_local_var_index(name) {
            Some(local_index) => context.code.push(Instruction::GetLocal(local_index)),
            None => {
                let name_index = self
                    .program
                    .get_or_assign_constant_pool_index(&ProgramObject::String(name.clone()));

                context.code.push(Instruction::GetGlobal(name_index));
            }
        }
    }

    fn compile_assign_variable(
        &mut self,
        context: &mut CompilationContext,
        name: &String,
        value: &AST,
    ) {
        self.compile_ast_node(context, value);

        match context.get_local_var_index(name) {
            Some(local_index) => context.code.push(Instruction::SetLocal(local_index)),
            None => {
                let name_index = self
                    .program
                    .get_or_assign_constant_pool_index(&ProgramObject::String(name.clone()));

                context.code.push(Instruction::SetGlobal(name_index));
            }
        }
    }

    fn compile_call_function(
        &mut self,
        context: &mut CompilationContext,
        name: &String,
        arguments: &Vec<Box<AST>>,
    ) {
        if arguments.len() > u8::MAX as usize {
            panic!("Maximum number of arguments exceeded");
        }

        for arg in arguments {
            self.compile_ast_node(context, arg)
        }

        let name_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::String(name.clone()));

        context.code.push(Instruction::CallFunction {
            name: name_index,
            arguments: arguments.len() as u8,
        })
    }

    fn compile_block(&mut self, context: &mut CompilationContext, nodes: &Vec<Box<AST>>) {
        context.extend_env();

        for node in nodes.iter().take(nodes.len() - 1) {
            self.compile_ast_node(context, node);
            context.code.push(Instruction::Drop);
        }

        nodes
            .last()
            .map(|node| self.compile_ast_node(context, node));

        context.pop_env();
    }

    fn compile_print(
        &mut self,
        context: &mut CompilationContext,
        format: &String,
        arguments: &Vec<Box<AST>>,
    ) {
        if arguments.len() > u8::MAX as usize {
            panic!("Maximum number of print arguments exceeded")
        }

        let format_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::String(format.clone()));

        for arg in arguments {
            self.compile_ast_node(context, arg);
        }

        context.code.push(Instruction::Print {
            format: format_index,
            arguments: arguments.len() as u8,
        })
    }

    fn compile_conditional(
        &mut self,
        context: &mut CompilationContext,
        condition: &AST,
        consequent: &AST,
        alternative: &AST,
    ) {
        let label_id = self.get_label_id();

        let consequent_label_name = format!("if:consequent:{}", label_id);
        let consequent_label_name_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::String(consequent_label_name));

        let end_label_name = format!("if:end:{}", label_id);
        let end_label_name_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::String(end_label_name));

        self.compile_ast_node(context, condition);

        context
            .code
            .push(Instruction::Branch(consequent_label_name_index));

        self.compile_ast_node(context, alternative);

        context.code.push(Instruction::Jump(end_label_name_index));
        context
            .code
            .push(Instruction::Label(consequent_label_name_index));

        self.compile_ast_node(context, consequent);

        context.code.push(Instruction::Label(end_label_name_index));
    }

    fn compile_loop(&mut self, context: &mut CompilationContext, condition: &AST, body: &AST) {
        let label_id = self.get_label_id();
        let body_label_name = format!("loop:body:{}", label_id);
        let body_label_name_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::String(body_label_name));

        let condition_label_name = format!("loop:condition:{}", label_id);
        let condition_label_name_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::String(condition_label_name));

        context
            .code
            .push(Instruction::Jump(condition_label_name_index));

        context.code.push(Instruction::Label(body_label_name_index));

        self.compile_ast_node(context, body);
        context.code.push(Instruction::Drop);

        context
            .code
            .push(Instruction::Label(condition_label_name_index));
        self.compile_ast_node(context, condition);

        context
            .code
            .push(Instruction::Branch(body_label_name_index));

        let null_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::Null);

        context.code.push(Instruction::Literal(null_index));
    }

    fn compile_method(
        &mut self,
        name: &String,
        parameters: &Vec<String>,
        body: &AST,
    ) -> ConstantPoolIndex {
        if parameters.len() > u8::MAX as usize {
            panic!("Maximum number of parameters exceeded");
        }

        let mut context = CompilationContext::new();
        context.extend_env();

        for parameter in parameters {
            context.add_local_var(parameter);
        }

        self.compile_ast_node(&mut context, body);
        context.code.push(Instruction::Return);

        self.program.add_method(
            name,
            parameters.len() as u8,
            context.max_locals_count - parameters.len() as u16,
            context.code,
        )
    }

    fn compile_call_method(
        &mut self,
        context: &mut CompilationContext,
        object: &AST,
        name: &String,
        arguments: &Vec<Box<AST>>,
    ) {
        if arguments.len() > (u8::MAX - 1) as usize {
            panic!("Maximum number of arguments exceeded");
        }

        self.compile_ast_node(context, object);

        for arg in arguments {
            self.compile_ast_node(context, arg);
        }

        let name_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::String(name.clone()));

        context.code.push(Instruction::CallMethod {
            name: name_index,
            arguments: (arguments.len() + 1) as u8,
        })
    }

    fn compile_object(
        &mut self,
        context: &mut CompilationContext,
        extends: &AST,
        member_nodes: &Vec<Box<AST>>,
    ) {
        self.compile_ast_node(context, extends);

        let members: Vec<ConstantPoolIndex> = member_nodes
            .iter()
            .map(|member| match member.as_ref() {
                AST::Variable { name, value } => {
                    self.compile_ast_node(context, value);

                    let name_index = self
                        .program
                        .get_or_assign_constant_pool_index(&ProgramObject::String(name.clone()));

                    self.program
                        .get_or_assign_constant_pool_index(&ProgramObject::Slot(name_index))
                }
                AST::Function {
                    name,
                    parameters,
                    body,
                } => {
                    let mut parameters_with_this = vec![String::from("this")];
                    parameters_with_this.extend_from_slice(parameters);

                    self.compile_method(name, &parameters_with_this, body)
                }
                _ => panic!(""),
            })
            .collect();

        let class_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::Class(members));

        context.code.push(Instruction::Object(class_index));
    }

    fn compile_access_field(
        &mut self,
        context: &mut CompilationContext,
        object: &AST,
        field: &String,
    ) {
        self.compile_ast_node(context, object);

        let field_name_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::String(field.clone()));

        context.code.push(Instruction::GetField(field_name_index));
    }

    fn compile_assign_field(
        &mut self,
        context: &mut CompilationContext,
        object: &AST,
        field: &String,
        value: &AST,
    ) {
        self.compile_ast_node(context, object);
        self.compile_ast_node(context, value);

        let field_name_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::String(field.clone()));

        context.code.push(Instruction::SetField(field_name_index));
    }

    fn declare_global_var(&mut self, name: &String) -> ConstantPoolIndex {
        let index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::String(name.clone()));

        let slot_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::Slot(index));

        self.program.register_global_var(name, slot_index);

        index
    }

    fn compile_simple_array(&mut self, context: &mut CompilationContext, size: &AST, value: &AST) {
        self.compile_ast_node(context, size);
        self.compile_ast_node(context, value);

        context.code.push(Instruction::Array);
    }

    fn compile_array(&mut self, context: &mut CompilationContext, size: &AST, value: &AST) {
        if !value.is_array_comprehensible() {
            self.compile_simple_array(context, size, value);
            return;
        }
        
        let var_id = self.get_label_id();

        let array_var_name = format!("::array:{}", var_id);
        let size_var_name = format!("::array_size:{}", var_id);
        let iter_var_name = format!("::array_iter:{}", var_id);

        let array_var_name_index = self.declare_global_var(&array_var_name);
        let size_var_name_index = self.declare_global_var(&size_var_name);
        let iter_var_name_index = self.declare_global_var(&iter_var_name);

        self.compile_literal(context, &ProgramObject::Integer(-1));

        context
            .code
            .push(Instruction::SetGlobal(iter_var_name_index));

        context.code.push(Instruction::Drop);

        self.compile_ast_node(context, size);

        context
            .code
            .push(Instruction::SetGlobal(size_var_name_index));

        self.compile_literal(context, &ProgramObject::Null);

        context.code.push(Instruction::Array);

        context
            .code
            .push(Instruction::SetGlobal(array_var_name_index));

        // iter <- iter + 1 < size
        let loop_condition = AST::CallMethod {
            object: Box::new(AST::AssignVariable {
                name: iter_var_name.clone(),
                value: Box::new(AST::CallMethod {
                    object: Box::new(AST::AccessVariable {
                        name: iter_var_name.clone(),
                    }),
                    name: "+".to_string(),
                    arguments: vec![Box::new(AST::Integer(1))],
                }),
            }),
            name: "<".to_string(),
            arguments: vec![Box::new(AST::AccessVariable {
                name: size_var_name,
            })],
        };

        // array[iter] <- value
        let loop_body = AST::CallMethod {
            object: Box::new(AST::AccessVariable {
                name: array_var_name,
            }),
            name: "set".to_string(),
            arguments: vec![
                Box::new(AST::AccessVariable {
                    name: iter_var_name,
                }),
                Box::new(value.clone()),
            ],
        };

        self.compile_loop(context, &loop_condition, &loop_body);

        context
            .code
            .push(Instruction::SetGlobal(array_var_name_index));

        context
            .code
            .push(Instruction::SetGlobal(size_var_name_index));

        context
            .code
            .push(Instruction::SetGlobal(iter_var_name_index));

        context.code.push(Instruction::Drop);
    }

    fn compile_array_access(&mut self, context: &mut CompilationContext, array: &AST, index: &AST) {
        self.compile_ast_node(context, array);
        self.compile_ast_node(context, index);

        let method_name_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::String(String::from("get")));

        context.code.push(Instruction::CallMethod {
            name: method_name_index,
            arguments: 2,
        });
    }

    fn compile_array_assign(
        &mut self,
        context: &mut CompilationContext,
        array: &AST,
        index: &AST,
        value: &AST,
    ) {
        self.compile_ast_node(context, array);
        self.compile_ast_node(context, index);
        self.compile_ast_node(context, value);

        let method_name_index = self
            .program
            .get_or_assign_constant_pool_index(&ProgramObject::String(String::from("set")));

        context.code.push(Instruction::CallMethod {
            name: method_name_index,
            arguments: 3,
        });
    }

    fn compile_ast_node(&mut self, context: &mut CompilationContext, node: &AST) {
        match node {
            AST::Null => self.compile_literal(context, &ProgramObject::Null),
            AST::Boolean(value) => self.compile_literal(context, &ProgramObject::Boolean(*value)),
            AST::Integer(value) => self.compile_literal(context, &ProgramObject::Integer(*value)),
            AST::Variable { name, value } => self.compile_variable(context, name, value),
            AST::AccessVariable { name } => self.compile_access_variable(context, name),
            AST::AssignVariable { name, value } => {
                self.compile_assign_variable(context, name, value)
            }
            AST::Function { .. } => panic!("Function can only be defined at the top level"),
            AST::CallFunction { name, arguments } => {
                self.compile_call_function(context, name, arguments)
            }
            AST::Top(_) => panic!("Top AST node cannot be a child node"),
            AST::Block(nodes) => self.compile_block(context, nodes),
            AST::Loop { condition, body } => self.compile_loop(context, condition, body),
            AST::Conditional {
                condition,
                consequent,
                alternative,
            } => self.compile_conditional(context, condition, consequent, alternative),
            AST::Print { format, arguments } => self.compile_print(context, format, arguments),
            AST::Array { size, value } => self.compile_array(context, size, value),
            AST::Object { extends, members } => self.compile_object(context, extends, members),
            AST::AccessField { object, field } => self.compile_access_field(context, object, field),
            AST::AccessArray { array, index } => self.compile_array_access(context, array, index),
            AST::AssignField {
                object,
                field,
                value,
            } => self.compile_assign_field(context, object, field, value),
            AST::AssignArray {
                array,
                index,
                value,
            } => self.compile_array_assign(context, array, index, value),
            AST::CallMethod {
                object,
                name,
                arguments,
            } => self.compile_call_method(context, object, name, arguments),
        }
    }

    fn compile_top(&mut self, top_nodes: &Vec<Box<AST>>) {
        let mut context = CompilationContext::new();

        for node in top_nodes {
            match node.as_ref() {
                AST::Function {
                    name,
                    parameters,
                    body,
                } => {
                    let method_index = self.compile_method(name, parameters, body);
                    self.program.add_function(method_index);
                }
                _ => {
                    self.compile_ast_node(&mut context, node);
                    context.code.push(Instruction::Drop);
                }
            };
        }

        self.program.add_entry_point(context.code, context.max_locals_count);
    }

    pub fn compile<W: Write>(mut self, ast: &AST, output: &mut W) {
        match ast {
            AST::Top(top_nodes) => self.compile_top(&top_nodes),
            _ => panic!("Root AST node must be a Top"),
        }

        self.program.write_as_bytes(output).panic_on_err();
    }
}
